﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using System.Globalization;
using System.Threading;

namespace WorkBuddy
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
           // DevExpress.XtraEditors.WindowsFormsSettings.RightToLeft = DevExpress.Utils.DefaultBoolean.True;
            BonusSkins.Register();
            string Lang = "ar-EG";

            CultureInfo cultureInfo = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(Lang);
            frm_Login.Instance.Show();
            Application.Run();
        }
    }
}
