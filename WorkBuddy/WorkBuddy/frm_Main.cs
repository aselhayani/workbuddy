﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WorkBuddy
{
    public partial class frm_Main : DevExpress.XtraEditors.XtraForm
    {
        private static frm_Main _Instance;
        public static frm_Main Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new frm_Main();
                }
                return _Instance;

            }
        }

        public frm_Main()
        {
            InitializeComponent();
            gridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
            gridView1.CustomDrawCell += GridView1_CustomDrawCell;

           
            RefreshData();
        }

        private void GridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
             
                var stage = gridView1.GetRowCellValue(e.RowHandle, "Stage");
                if (stage != null)
                {
                    int stageID = Convert.ToInt16(stage);
                    if (stageID == 0)
                    {
                        Pen pen = new Pen(Color.FromArgb(80, Color.Red));
                        e.Graphics.DrawRectangle(pen, Rectangle.Inflate(e.Bounds, -1, -1));
                    }
                    else if (stageID == 2)
                    {
                        Pen pen = new Pen(Color.FromArgb(80, Color.Orange));
                        e.Graphics.DrawRectangle(pen, Rectangle.Inflate(e.Bounds, -1, -1));
                    }
                }
             
            if (e.Column.FieldName == "Priorty")
            {
                int val = 0;
                int.TryParse(e.CellValue.ToString(), out val);

                if (val == 0)
                {
                    e.Appearance.BackColor = Color.Red;

                }
                if (val == 1)
                {
                    e.Appearance.BackColor = Color.DarkOrange;

                }
                if (val == 2)
                { 
                    e.Appearance.BackColor = Color.Orange;

                }
                if (val == 3)
                { 
                    e.Appearance.BackColor = Color.Yellow;

                }
                if (val == 4)
                { 
                    e.Appearance.BackColor = Color.LightYellow;
                }
            }
        }

        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            
            if (e.Column.FieldName == "ToUser")
            {
                if(Convert.ToInt32( e.Value) == 0) 
                    e.DisplayText = "الكل";
                else
                {
                    WorkBuddyDataContext db = new WorkBuddyDataContext();
                    var userName = db.Users.Single(x => x.ID == Convert.ToInt32(e.Value)).Name;
                    e.DisplayText = userName;
                }
            }
            
            
            else if (e.Column.FieldName == "Priorty")
            {
                int val = 0;
                int.TryParse(e.Value. ToString(), out val);

                if (val == 0)
                {
                    e.DisplayText = "عاجل";
                   
                }
                if (val == 1)
                {
                    e.DisplayText = "هام جدا ";
                    
                }
                if (val == 2)
                {
                    e.DisplayText = "هام";
                     
                }
                if (val == 3)
                {
                    e.DisplayText = "عادي";
                    
                }
                if (val == 4)
                {
                    e.DisplayText = "غير هام  ";
                    
                }
            }
        }

        void RefreshData()
        {
            WorkBuddyDataContext db = new WorkBuddyDataContext();
            var source = from t in db.Tasks
                         from fu in db.Users.Where(x => x.ID == t.UserID)
                         from tu in db.Users.Where(x => x.ID == t.ToUserID).DefaultIfEmpty()
                         where t.Stage != 3
                         select new
                         {
                             t.ID,
                             t.Caption,
                             t.Descreption,
                             t.Date,
                             t.DueDate,
                             t.Priorty,
                             t.Stage,
                             User = fu.Name,
                             ToUser = t.ToUserID ,
                             //   t.Tags ,
                         };
            gridControl1.DataSource = source;

        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frm_AddTask.Instance.ShowDialog();
            RefreshData();
        }

        private void frm_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void frm_Main_Load(object sender, EventArgs e)
        {
            gridView1.Columns["ID"].Visible = false;
            gridView1.Columns["Date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridView1.Columns["Date"].DisplayFormat.FormatString = "dddd dd-mm";
        }
    }
}