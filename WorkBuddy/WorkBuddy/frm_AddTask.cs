﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WorkBuddy
{
    public partial class frm_AddTask : DevExpress.XtraEditors.XtraForm
    {
        public  User user;
        public Task task;
        private static frm_AddTask _Instance;
        public static frm_AddTask Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new frm_AddTask();
                }
                return _Instance;

            }
        }
        
        public frm_AddTask()
        {
            InitializeComponent();
            //xtraScrollableControl1.AutoScrollMinSize = new Size(0, 1);
            //xtraScrollableControl1.AutoScrollMargin  = new Size(15, 15);
            //xtraScrollableControl1.AutoScroll = true; 
            //xtraScrollableControl1.HorizontalScroll.Visible = false ;
            //xtraScrollableControl1.VerticalScroll.Visible = true; 
            RefreshData();
         
        }
        void RefreshData()
        {
            WorkBuddyDataContext db = new WorkBuddyDataContext();
           
            lookUpEdit1.Properties.DataSource = db.Users.ToList();
            ((List<User>)lookUpEdit1.Properties.DataSource).Add(new User()
            {
                ID = 0 ,
                Name = "الكل",
                
            });
            lookUpEdit1.Properties.DisplayMember = "Name";
            lookUpEdit1.Properties.ValueMember  = "ID";
            lookUpEdit1.Properties.PopulateViewColumns(); 
            lookUpEdit1.Properties.View.Columns[0].Visible = false;
            lookUpEdit1.Properties.View.Columns[2].Visible = false;
            lookUpEdit1.Properties.View.Columns[3].Visible = false;
        }

        private void View_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = Color.FromArgb(((Tag)((GridView)sender).GetRow(e.RowHandle)).Color);
        }

        private void frm_Main_Load(object sender, EventArgs e)
        {
            simpleButton2_Click(null, null);


        }

     
       

     
        
       

        private void Button_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //foreach (var item in xtraScrollableControl1.Controls)
            //{
            //    if (item .GetType() == typeof(ButtonEdit))
            //    {
            //        xtraScrollableControl1.Controls.Remove((ButtonEdit)item);
            //        ((ButtonEdit)item).Dispose();
            //    }
            //}
            ((ButtonEdit)sender).Dispose();
        }

     
       
        bool ChangesMade;
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            task = new Task()
            {
                Date = DateTime.Now,
                UserID = user.ID ,
                Priorty = 3 
            };
            GetDate();
            ChangesMade = false;
        }
        void GetDate()
        {
            comboBoxEdit3.SelectedIndex = task.Type;
            comboBoxEdit2.SelectedIndex = task.Priorty;
            dateEdit2.DateTime = task.DueDate;
            textEdit1.Text = task.Caption;
            memoEdit1.Text = task.Descreption;
            lookUpEdit1.EditValue = task.ToUserID;
            dateEdit1.DateTime = task.Date;
      
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if(dateEdit2.DateTime.Year < 2000)
            {
                dateEdit2.ErrorText = "برجاء ادخال التاريخ";
                return;
            }  
            if(comboBoxEdit2.SelectedIndex < 0)
            {
                comboBoxEdit2.ErrorText = "مطلوب";
                return;
            }
            if (comboBoxEdit3 .SelectedIndex < 0)
            {
                comboBoxEdit3.ErrorText = "مطلوب";
                return;
            }
            if (lookUpEdit1 .EditValue == null )
            {
                lookUpEdit1.ErrorText = "مطلوب";
                return;
            }
            if (textEdit1 .Text.Trim()  == string.Empty )
            {
                textEdit1.ErrorText = "مطلوب";
                return;
            }
            WorkBuddyDataContext db = new WorkBuddyDataContext();
            if (task .ID == 0)
            {
                db.Tasks.InsertOnSubmit(task);
            }
            else
            {
                task = db.Tasks.Single(x => x.ID == task.ID);
            }
            task.Type =Convert.ToByte(   comboBoxEdit3.SelectedIndex  );
            task.Priorty = Convert.ToByte(comboBoxEdit2.SelectedIndex);
            task.DueDate= dateEdit2.DateTime;
            task.Caption = textEdit1.Text;
            task.Descreption = memoEdit1.Text ;
            task.ToUserID  =Convert.ToInt32( lookUpEdit1.EditValue );
            task.Date=  dateEdit1.DateTime;
       
            db.SubmitChanges(); 
            this.Close();
        }
    }
}
