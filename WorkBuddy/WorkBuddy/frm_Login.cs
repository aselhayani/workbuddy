﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WorkBuddy
{
    public partial class frm_Login : DevExpress.XtraEditors.XtraForm
    {
        private static frm_Login _Instance;
        public static frm_Login Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new frm_Login();
                }
                return _Instance;

            }
        }
        public frm_Login()
        {
            InitializeComponent();
            WorkBuddyDataContext db = new WorkBuddyDataContext(Properties.Settings.Default.WorkBuddyConnectionString);
            var users = db.Users.Select(x => x.UserName).ToList();
            textEdit1.MaskBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textEdit1.MaskBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            users.ForEach(x =>
            {
                textEdit1.MaskBox.AutoCompleteCustomSource.Add(x);
            }); 
        }

        private void frm_Login_SizeChanged(object sender, EventArgs e)
        {
            if(this.WindowState == FormWindowState.Maximized )
            this.WindowState = FormWindowState.Normal;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            WorkBuddyDataContext db = new WorkBuddyDataContext(Properties.Settings.Default.WorkBuddyConnectionString);
            var user = db.Users.FirstOrDefault(x => x.UserName == textEdit1.Text.Trim() && x.Password == textEdit2.Text.Trim());
            if (user != null )
            {
                frm_AddTask.Instance.user = user;
                frm_Main .Instance.Show();
                this.Close();
            }else
            {
                XtraMessageBox.Show("اسم المستخدم او كلمه السر خاطئه ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
    }
}