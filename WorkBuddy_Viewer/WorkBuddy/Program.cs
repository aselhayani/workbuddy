﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkBuddy
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            WorkBuddyDataContext db = new WorkBuddyDataContext();
            
            string Lang = "ar-EG";

            CultureInfo cultureInfo = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(Lang);
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(Lang);
            Application.Run(new Form1());
        }
    }
}
