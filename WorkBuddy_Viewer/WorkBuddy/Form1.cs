﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.CustomEditor;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkBuddy
{
    public partial class Form1 : XtraForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshData();
            layoutView1.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.MultiColumn ;
            layoutView1.Columns["ID"].Visible = false;
            layoutView1.Columns["ID"].Visible = false;
            layoutView1.Columns["Caption"].Visible = false;
            layoutView1.Columns["Stage"].Visible = false;

            layoutView1.CardCaptionFormat = "{3}";

            RepositoryItemMemoEdit edit = new RepositoryItemMemoEdit();
            gridControl1.RepositoryItems.Add(edit);

            layoutView1.Columns["Descreption"].ColumnEdit = edit;

            RepositoryItemAnyControl customControl = new RepositoryItemAnyControl();
            TagControl tagControl = new TagControl();
            customControl.Control = tagControl;
            gridControl1.RepositoryItems.Add(customControl);

            // layoutView1.Columns["Tags"].ColumnEdit = customControl; ;
            layoutView1.Columns["Caption"].Caption = "العنوان";
            layoutView1.Columns["Descreption"].Caption = "الوصف";
            layoutView1.Columns["Date"].Caption = "ت الاضافه";
            layoutView1.Columns["DueDate"].Caption = "ت النهايه";
            layoutView1.Columns["Priorty"].Caption = "الاهميه";
            layoutView1.Columns["User"].Caption = "من";
            layoutView1.Columns["ToUser"].Caption = "الي";

            layoutView1.Columns["Date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            layoutView1.Columns["Date"].DisplayFormat.FormatString = "dddd dd-mm";

            layoutView1.Columns["DueDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            layoutView1.Columns["DueDate"].DisplayFormat.FormatString = "dd-MM-yyyy";

            layoutView1.Columns["Descreption"].LayoutViewField.TextVisible = false;
            layoutView1.Columns["DueDate"].LayoutViewField.TextVisible = false;
            layoutView1.Columns["Date"].LayoutViewField.TextVisible = false;
            layoutView1.Columns["Priorty"].LayoutViewField.TextVisible = false;

            // layoutView1.Columns["Tags"].LayoutViewField.TextVisible = false; 
            layoutView1.CustomDrawCardFieldValue += cardView1_CustomDrawCardFieldValue;
            layoutView1.CustomDrawCardCaption += LayoutView1_CustomDrawCardCaption;
            layoutView1.Appearance.CardCaption.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            layoutView1.Appearance.CardCaption.Options.UseFont = true;
            layoutView1.Appearance.CardCaption.Options.UseTextOptions = true;
            layoutView1.Appearance.FieldCaption.Font = new System.Drawing.Font("Tahoma", 14, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            layoutView1.Appearance.FieldCaption.ForeColor = System.Drawing.Color.Gray;
            layoutView1.Appearance.FieldCaption.Options.UseFont = true;
            layoutView1.Appearance.FieldCaption.Options.UseForeColor = true;
            layoutView1.Appearance.FieldValue.Font = new System.Drawing.Font("Tahoma", 14, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            layoutView1.Appearance.FieldValue.Options.UseFont = true;

        }
        private void LayoutView1_CustomDrawCardCaption(object sender, DevExpress.XtraGrid.Views.Layout.Events.LayoutViewCustomDrawCardCaptionEventArgs e)
        {
            e.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

        }

        private void cardView1_CustomDrawCardFieldValue(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {


            //else if (e.Column.FieldName == "Caption")
            //{
            //     e.Appearance.Font =  new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //}
            if (e.Column.FieldName == "ToUser")
            {
                if (Convert.ToInt32(e.CellValue) == 0)
                    e.DisplayText = "الكل";
                else
                {
                    WorkBuddyDataContext db = new WorkBuddyDataContext();
                    var userName = db.Users.Single(x => x.ID == Convert.ToInt32(e.CellValue)).Name;
                    e.DisplayText = userName;
                }
            }
            else if (e.Column.FieldName == "DueDate")
            {
                var date = e.CellValue as DateTime?;
                if (date.HasValue)
                {
                    if ((date.Value - DateTime.Now).Days <= 0)
                        e.Appearance.BackColor = Color.Red;
                    else if ((date.Value - DateTime.Now).Days <= 1)
                        e.Appearance.BackColor = Color.Orange;
                    else if ((date.Value - DateTime.Now).Days <= 2)
                        e.Appearance.BackColor = Color.Yellow;
                }
            }
            else if (e.Column.FieldName == "Priorty")
            {
                int val = 0;
                int.TryParse(e.CellValue.ToString(), out val);

                if (val == 0)
                {
                    e.DisplayText = "عاجل";
                    e.Appearance.BackColor = Color.Red;

                }
                if (val == 1)
                {
                    e.DisplayText = "هام جدا ";
                    e.Appearance.BackColor = Color.DarkOrange;

                }
                if (val == 2)
                {
                    e.DisplayText = "هام";
                    e.Appearance.BackColor = Color.Orange;

                }
                if (val == 3)
                {
                    e.DisplayText = "عادي";
                    e.Appearance.BackColor = Color.Yellow;

                }
                if (val == 4)
                {
                    e.DisplayText = "غير هام  ";
                    e.Appearance.BackColor = Color.LightYellow;

                }
            }
        }

        private void layoutView1_CustomDrawCardBackground(object sender, DevExpress.XtraGrid.Views.Layout.Events.LayoutViewCustomDrawCardBackgroundEventArgs e)
        {
            var stage = layoutView1.GetRowCellValue(e.RowHandle, "Stage");
            if(stage != null)
            {
                int stageID = Convert.ToInt16(stage);
                if(stageID == 0)
                {
                    e.DefaultDraw();

                    using (var highlight = new SolidBrush(Color.FromArgb(25, Color.Red)))
                    {

                        // Fill card with semi-transparent color 

                        e.Cache.FillRectangle(highlight, Rectangle.Inflate(e.Bounds, -1, -1));

                    }

                }
                else
                {
                    
                }
            }
        }
        void RefreshData()
        {
            WorkBuddyDataContext db = new WorkBuddyDataContext();
            var source = from t in db.Tasks
                         from fu in db.Users.Where(x => x.ID == t.UserID)
                         from tu in db.Users.Where(x => x.ID == t.ToUserID).DefaultIfEmpty()
                         where t.Stage != 3
                         select new
                         {
                             t.ID,
                             t.Caption,
                             t.Descreption,
                             t.Date,
                             t.DueDate,
                             t.Priorty,
                             t.Stage,
                             User = fu.Name,
                             ToUser = t.ToUserID,
                             //   t.Tags ,
                         };
            gridControl1.DataSource = source.ToList();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}
