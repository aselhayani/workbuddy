﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils.Drawing;

namespace WorkBuddy
{
    public interface IAnyControlEdit
    {
        object EditValue { get; set; }
        bool SupportsDraw { get; }
        bool AllowBorder { get; }
        event EventHandler EditValueChanged;
        Size CalcSize(Graphics g);
        void Draw(GraphicsCache cache, AnyControlEditViewInfo viewInfo);
        void SetupAsDrawControl();
        void SetupAsEditControl();
        string GetDisplayText(object editValue);
        bool IsNeededKey(KeyEventArgs e);
        bool AllowClick(Point point);
    }

}