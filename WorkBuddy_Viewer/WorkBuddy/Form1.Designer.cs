﻿namespace WorkBuddy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tasksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutView1 = new DevExpress.XtraGrid.Views.Layout.LayoutView();
            this.colID = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colCaption = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colCaption = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDescreption = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDescreption = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colUserID = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colUserID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colToUserID = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colToUserID = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDate = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDate = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colStage = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colStage = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colDueDate = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colDueDate = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.colPriorty = new DevExpress.XtraGrid.Columns.LayoutViewColumn();
            this.layoutViewField_colPriorty = new DevExpress.XtraGrid.Views.Layout.LayoutViewField();
            this.layoutViewCard1 = new DevExpress.XtraGrid.Views.Layout.LayoutViewCard();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCaption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDescreption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colToUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colStage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPriorty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tasksBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 12);
            this.gridControl1.MainView = this.layoutView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(642, 381);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.layoutView1});
            // 
            // tasksBindingSource
            // 
            this.tasksBindingSource.DataMember = "Tasks";
            // 
            // layoutView1
            // 
            this.layoutView1.Appearance.CardCaption.Options.UseTextOptions = true;
            this.layoutView1.Appearance.CardCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutView1.Appearance.FieldValue.Options.UseTextOptions = true;
            this.layoutView1.Appearance.FieldValue.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.LayoutViewColumn[] {
            this.colID,
            this.colCaption,
            this.colDescreption,
            this.colUserID,
            this.colToUserID,
            this.colDate,
            this.colStage,
            this.colDueDate,
            this.colPriorty});
            this.layoutView1.GridControl = this.gridControl1;
            this.layoutView1.Name = "layoutView1";
            this.layoutView1.OptionsBehavior.Editable = false;
            this.layoutView1.OptionsSingleRecordMode.CardAlignment = DevExpress.XtraGrid.Views.Layout.CardsAlignment.Near;
            this.layoutView1.OptionsView.CardsAlignment = DevExpress.XtraGrid.Views.Layout.CardsAlignment.Near;
            this.layoutView1.OptionsView.ContentAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutView1.TemplateCard = this.layoutViewCard1;
            this.layoutView1.CustomDrawCardBackground += new DevExpress.XtraGrid.Views.Layout.Events.LayoutViewCustomDrawCardBackgroundEventHandler(this.layoutView1_CustomDrawCardBackground);
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.LayoutViewField = this.layoutViewField_colID;
            this.colID.Name = "colID";
            // 
            // layoutViewField_colID
            // 
            this.layoutViewField_colID.EditorPreferredWidth = 120;
            this.layoutViewField_colID.Location = new System.Drawing.Point(0, 0);
            this.layoutViewField_colID.Name = "layoutViewField_colID";
            this.layoutViewField_colID.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colID.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colCaption
            // 
            this.colCaption.FieldName = "Caption";
            this.colCaption.LayoutViewField = this.layoutViewField_colCaption;
            this.colCaption.Name = "colCaption";
            // 
            // layoutViewField_colCaption
            // 
            this.layoutViewField_colCaption.EditorPreferredWidth = 120;
            this.layoutViewField_colCaption.Location = new System.Drawing.Point(0, 24);
            this.layoutViewField_colCaption.Name = "layoutViewField_colCaption";
            this.layoutViewField_colCaption.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colCaption.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colDescreption
            // 
            this.colDescreption.FieldName = "Descreption";
            this.colDescreption.LayoutViewField = this.layoutViewField_colDescreption;
            this.colDescreption.Name = "colDescreption";
            // 
            // layoutViewField_colDescreption
            // 
            this.layoutViewField_colDescreption.EditorPreferredWidth = 120;
            this.layoutViewField_colDescreption.Location = new System.Drawing.Point(0, 48);
            this.layoutViewField_colDescreption.Name = "layoutViewField_colDescreption";
            this.layoutViewField_colDescreption.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colDescreption.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colUserID
            // 
            this.colUserID.FieldName = "User";
            this.colUserID.LayoutViewField = this.layoutViewField_colUserID;
            this.colUserID.Name = "colUserID";
            // 
            // layoutViewField_colUserID
            // 
            this.layoutViewField_colUserID.EditorPreferredWidth = 120;
            this.layoutViewField_colUserID.Location = new System.Drawing.Point(0, 72);
            this.layoutViewField_colUserID.Name = "layoutViewField_colUserID";
            this.layoutViewField_colUserID.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colUserID.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colToUserID
            // 
            this.colToUserID.FieldName = "ToUser";
            this.colToUserID.LayoutViewField = this.layoutViewField_colToUserID;
            this.colToUserID.Name = "colToUserID";
            // 
            // layoutViewField_colToUserID
            // 
            this.layoutViewField_colToUserID.EditorPreferredWidth = 120;
            this.layoutViewField_colToUserID.Location = new System.Drawing.Point(0, 96);
            this.layoutViewField_colToUserID.Name = "layoutViewField_colToUserID";
            this.layoutViewField_colToUserID.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colToUserID.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colDate
            // 
            this.colDate.FieldName = "Date";
            this.colDate.LayoutViewField = this.layoutViewField_colDate;
            this.colDate.Name = "colDate";
            // 
            // layoutViewField_colDate
            // 
            this.layoutViewField_colDate.EditorPreferredWidth = 120;
            this.layoutViewField_colDate.Location = new System.Drawing.Point(0, 120);
            this.layoutViewField_colDate.Name = "layoutViewField_colDate";
            this.layoutViewField_colDate.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colDate.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colStage
            // 
            this.colStage.FieldName = "Stage";
            this.colStage.LayoutViewField = this.layoutViewField_colStage;
            this.colStage.Name = "colStage";
            // 
            // layoutViewField_colStage
            // 
            this.layoutViewField_colStage.EditorPreferredWidth = 120;
            this.layoutViewField_colStage.Location = new System.Drawing.Point(0, 144);
            this.layoutViewField_colStage.Name = "layoutViewField_colStage";
            this.layoutViewField_colStage.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colStage.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colDueDate
            // 
            this.colDueDate.FieldName = "DueDate";
            this.colDueDate.LayoutViewField = this.layoutViewField_colDueDate;
            this.colDueDate.Name = "colDueDate";
            // 
            // layoutViewField_colDueDate
            // 
            this.layoutViewField_colDueDate.EditorPreferredWidth = 120;
            this.layoutViewField_colDueDate.Location = new System.Drawing.Point(0, 168);
            this.layoutViewField_colDueDate.Name = "layoutViewField_colDueDate";
            this.layoutViewField_colDueDate.Size = new System.Drawing.Size(224, 24);
            this.layoutViewField_colDueDate.TextSize = new System.Drawing.Size(61, 13);
            // 
            // colPriorty
            // 
            this.colPriorty.FieldName = "Priorty";
            this.colPriorty.LayoutViewField = this.layoutViewField_colPriorty;
            this.colPriorty.Name = "colPriorty";
            // 
            // layoutViewField_colPriorty
            // 
            this.layoutViewField_colPriorty.EditorPreferredWidth = 120;
            this.layoutViewField_colPriorty.Location = new System.Drawing.Point(0, 192);
            this.layoutViewField_colPriorty.Name = "layoutViewField_colPriorty";
            this.layoutViewField_colPriorty.Size = new System.Drawing.Size(224, 44);
            this.layoutViewField_colPriorty.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutViewCard1
            // 
            this.layoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutViewCard1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutViewField_colID,
            this.layoutViewField_colCaption,
            this.layoutViewField_colDescreption,
            this.layoutViewField_colUserID,
            this.layoutViewField_colToUserID,
            this.layoutViewField_colDate,
            this.layoutViewField_colStage,
            this.layoutViewField_colDueDate,
            this.layoutViewField_colPriorty});
            this.layoutViewCard1.Name = "layoutViewCard1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(666, 405);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(666, 405);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(646, 385);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 405);
            this.Controls.Add(this.layoutControl1);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colCaption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDescreption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colToUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colStage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewField_colPriorty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutViewCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Layout.LayoutView layoutView1;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewCard layoutViewCard1; 
        private System.Windows.Forms.BindingSource tasksBindingSource; 
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colID;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colID;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colCaption;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colCaption;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colDescreption;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colDescreption;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colUserID;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colUserID;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colToUserID;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colToUserID;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colDate;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colDate;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colStage;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colStage;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colDueDate;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colDueDate;
        private DevExpress.XtraGrid.Columns.LayoutViewColumn colPriorty;
        private DevExpress.XtraGrid.Views.Layout.LayoutViewField layoutViewField_colPriorty;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.Timer timer1;
    }
}

