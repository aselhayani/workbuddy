﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils.Drawing;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.Linq;
using System.Diagnostics;
using DevExpress.XtraRichEdit.API.Native;
using System.Collections.Generic;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Layout.Export;

namespace WorkBuddy
{
    public partial class TagControl : UserControl, IAnyControlEdit
    {
        private string tags = "1";
       
        public TagControl()
        {
            InitializeComponent();
            this.richEditControl1.Options.HorizontalRuler.ShowLeftIndent = false;
            this.richEditControl1.Options.HorizontalRuler.ShowRightIndent = false;
            this.richEditControl1.Options.HorizontalRuler.ShowTabs = false;
            this.richEditControl1.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditControl1.Options.HorizontalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden;
            this.richEditControl1.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEditControl1.Options.VerticalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden;
            richEditControl1.CustomMarkDraw += richEditControl1_CustomMarkDraw;
            richEditControl1.SearchFormShowing += richEditControl1_SearchFormShowing;
        }
        void richEditControl1_SearchFormShowing(object sender, SearchFormShowingEventArgs e)
        {
            e.Handled = true;
            //CustomSearchForm form = new CustomSearchForm(e.ControllerParameters);
            //e.DialogResult = form.ShowDialog();
        }

        void richEditControl1_CustomMarkDraw(object sender, RichEditCustomMarkDrawEventArgs e)
        {
            if (e.VisualInfoCollection != null)
            {
                Dictionary<DocumentRange, Rectangle> table = new Dictionary<DocumentRange, Rectangle>();
                foreach (CustomMarkVisualInfo viewInfo in e.VisualInfoCollection)
                {
                    DocumentRange range = viewInfo.UserData as DocumentRange;
                    if (!table.ContainsKey(range))
                        table.Add(range, Rectangle.Empty);

                    DevExpress.XtraRichEdit.API.Native.CustomMark mark = this.richEditControl1.Document.CustomMarks.GetByVisualInfo(viewInfo);
                    Rectangle bounds = table[range];
                    Rectangle newBounds;
                    if (mark.Position == range.Start)
                        newBounds = Rectangle.FromLTRB(viewInfo.Bounds.Left, viewInfo.Bounds.Top, bounds.Right, bounds.Bottom);
                    else
                        newBounds = Rectangle.FromLTRB(bounds.Left, bounds.Top, viewInfo.Bounds.Right, viewInfo.Bounds.Bottom);
                    table[range] = newBounds;
                }
                using (Brush brush = new SolidBrush(Color.FromArgb(100, Color.Red )))
                {
                    foreach (Rectangle rect in table.Values)
                    {
                        e.Graphics.FillRectangle(brush, rect);
                    }
                }
            }
        }


        private void Marktext(string text )
        {


            DocumentRange[] ranges = this.richEditControl1.Document.FindAll(text, DevExpress.XtraRichEdit.API.Native.SearchOptions.WholeWord);
            if (ranges != null && ranges.Length > 0)
            {
                foreach (DocumentRange range in ranges)
                {
                    this.richEditControl1.Document.CustomMarks.Create(range.Start, range);
                    this.richEditControl1.Document.CustomMarks.Create(range.End, range);
                }
            }

        }
        void AddTag(int tagID)
        {
            AddTag((new WorkBuddyDataContext(Properties.Settings.Default.WorkBuddyConnectionString)).Tags.Single(x => x.ID == tagID));
        }

        void AddTag(Tag tag)
        {
            ButtonEdit button = new ButtonEdit();
            button.Dock = DockStyle.Left;
            button.Name = "button" + tag.ID.ToString();
            button.Text = tag.TagName;
            button.BackColor = Color.FromArgb(tag.Color);
            button.Properties.Buttons.Clear();
            button.Tag = tag.ID;
            button.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            button.ButtonClick += Button_ButtonClick;
            button.Properties.Buttons.Add(new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear));
            var size = TextRenderer.MeasureText(button.Text, button.Font);
            button.Size = new Size(size.Width, size.Height);
            richEditControl1.Text += tag.TagName + " / ";
            Marktext(tag.TagName);
            //var item = layoutControlGroup1.AddItem();
            //item.SizeConstraintsType = SizeConstraintsType.Custom;
            //item.Control = button;
            //item.TextVisible = false;
            //item.Size = button.Size;

        }

        private void Button_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
           
            ((ButtonEdit)sender).Dispose();
        }

        public object EditValue
        {
            get
            {
                return tags ;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                tags =(string) value;
                richEditControl1.Text = "" ;
                var ls = tags.Split(new char[] { Convert.ToChar(",") }, options: StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in ls)
                {
                    AddTag(Convert.ToInt32(item));
                }
                Invalidate();
                Refresh();
            }
        }

        public bool SupportsDraw
        {
            get { return false; }
        }

        public bool AllowBorder
        {
            get { return false; }
        }

        public event EventHandler EditValueChanged;

        public Size CalcSize(Graphics g)
        {
            return Size;
        }

        public void Draw(GraphicsCache cache, AnyControlEditViewInfo viewInfo)
        {
            throw new NotImplementedException();
        }

        public void SetupAsDrawControl()
        {
            //throw new NotImplementedException();
        }

        public void SetupAsEditControl()
        {
            throw new NotImplementedException();
        }

        public string GetDisplayText(object editValue)
        {
            return editValue == null ? string.Empty : editValue.ToString();
        }

        public bool IsNeededKey(KeyEventArgs e)
        {
            return true ;
        }

        public bool AllowClick(Point point)
        {
            return true;
        }

   

        private void layoutControlGroup1_Showing(object sender, EventArgs e)
        {

        }

    
    }
}