﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;

namespace WorkBuddy
{
    [UserRepositoryItem("Register")]
    public class RepositoryItemAnyControl : RepositoryItem
    {
        internal const string EditorName = "AnyControlEdit";
        private IAnyControlEdit control;
        private IAnyControlEdit drawControlInstance;

        static RepositoryItemAnyControl()
        {
            Register();
        }

        public override string EditorTypeName
        {
            get { return EditorName; }
        }

        public IAnyControlEdit Control
        {
            get { return control; }
            set
            {
                if (!(value is Control)) value = null;
                if (control == value) return;
                if (control != null) OnControlRelease(control);
                control = value;
                if (control != null) OnControlSubscribe(control);
                OnControlChanged();
            }
        }

        public override BorderStyles BorderStyle
        {
            get
            {
                if (Control != null && !Control.AllowBorder) return BorderStyles.NoBorder;
                return base.BorderStyle;
            }
            set { base.BorderStyle = value; }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new AnyControlEdit OwnerEdit
        {
            get { return base.OwnerEdit as AnyControlEdit; }
        }

        internal bool AllowDisposeControl { get; set; }

        public static void Register()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName, typeof(AnyControlEdit),
                typeof(RepositoryItemAnyControl), typeof(AnyControlEditViewInfo),
                new AnyControlEditPainter(), true, null));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                IAnyControlEdit control = Control;
                Control = null;
                if (AllowDisposeControl) Dispose(control);
            }
            base.Dispose(disposing);
        }

        public override void Assign(RepositoryItem item)
        {
            base.Assign(item);
            var source = item as RepositoryItemAnyControl;
            if (source == null) return;
            if (OwnerEdit != null)
            {
                Control = source.CreateControlInstance();
            }
            else
                Control = source.Control;
            //todo
        }

        //protected override bool IsNeededKeyCore(Keys keyData)
        //{
        //    return base.IsNeededKeyCore(keyData);
        //}
        protected override bool NeededKeysContains(Keys key)
        {
            if (base.NeededKeysContains(key)) return true;
            switch (key)
            {
                case Keys.F2:
                case Keys.A:
                case Keys.Add:
                case Keys.B:
                case Keys.Back:
                case Keys.C:
                case Keys.Clear:
                case Keys.D:
                case Keys.D0:
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
                case Keys.D5:
                case Keys.D6:
                case Keys.D7:
                case Keys.D8:
                case Keys.D9:
                case Keys.Decimal:
                case Keys.Delete:
                case Keys.Divide:
                case Keys.E:
                case Keys.End:
                case Keys.F:
                case Keys.F20:
                case Keys.G:
                case Keys.H:
                case Keys.Home:
                case Keys.I:
                case Keys.Insert:
                case Keys.J:
                case Keys.K:
                case Keys.L:
                case Keys.Left:
                case Keys.M:
                case Keys.Multiply:
                case Keys.N:
                case Keys.NumPad0:
                case Keys.NumPad1:
                case Keys.NumPad2:
                case Keys.NumPad3:
                case Keys.NumPad4:
                case Keys.NumPad5:
                case Keys.NumPad6:
                case Keys.NumPad7:
                case Keys.NumPad8:
                case Keys.NumPad9:
                case Keys.Alt:
                case (Keys.RButton | Keys.ShiftKey):
                case Keys.O:
                case Keys.Oem8:
                case Keys.OemBackslash:
                case Keys.OemCloseBrackets:
                case Keys.Oemcomma:
                case Keys.OemMinus:
                case Keys.OemOpenBrackets:
                case Keys.OemPeriod:
                case Keys.OemPipe:
                case Keys.Oemplus:
                case Keys.OemQuestion:
                case Keys.OemQuotes:
                case Keys.OemSemicolon:
                case Keys.Oemtilde:
                case Keys.P:
                case Keys.Q:
                case Keys.R:
                case Keys.Right:
                case Keys.S:
                case Keys.Space:
                case Keys.Subtract:
                case Keys.T:
                case Keys.U:
                case Keys.V:
                case Keys.W:
                case Keys.X:
                case Keys.Y:
                case Keys.Z:
                    return true;
            }
            return false;
        }

        protected virtual void OnControlChanged()
        {
            OnPropertiesChanged();
        }


        protected virtual void OnControlSubscribe(IAnyControlEdit control)
        {
            if (OwnerEdit != null) OwnerEdit.OnControlSubscribe(control);
        }

        public override BaseEdit CreateEditor()
        {
            var editor = base.CreateEditor() as AnyControlEdit;
            if (Control != null)
            {
                editor.Properties.Control = CreateControlInstance();
                editor.Properties.AllowDisposeControl = true;
            }
            return editor;
        }

        protected virtual void OnControlRelease(IAnyControlEdit control)
        {
            Dispose(drawControlInstance);
            drawControlInstance = null;
            if (OwnerEdit != null) OwnerEdit.OnControlRelease(control);
        }

        protected internal IAnyControlEdit GetDrawControlInstance()
        {
            if (OwnerEdit != null) return Control;
            EnsureDrawControlInstance();
            return drawControlInstance;
        }

        internal IAnyControlEdit CreateControlInstance()
        {
            if (Control != null) return Activator.CreateInstance(Control.GetType()) as IAnyControlEdit;
            return null;
        }

        protected internal void EnsureDrawControlInstance()
        {
            if (drawControlInstance == null && Control != null)
            {
                drawControlInstance = CreateControlInstance();
                drawControlInstance.SetupAsDrawControl();
            }
        }

        private void Dispose(IAnyControlEdit control)
        {
            var disposable = control as IDisposable;
            if (disposable != null) disposable.Dispose();
        }
    }
}